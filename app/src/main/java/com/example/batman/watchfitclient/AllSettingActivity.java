package com.example.batman.watchfitclient;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.batman.watchfitclient.Data.data.KigDbHelper;
import com.example.batman.watchfitclient.Data.data.OsmDbHelper;
import com.example.batman.watchfitclient.Data.data.TrainingContract;
import com.example.batman.watchfitclient.Data.data.TrainingDbHelper;


public class AllSettingActivity {
    private OsmDbHelper mDbHelper1;
    private TrainingDbHelper mDbHelper2;
    private KigDbHelper mDbHelper3;
    FragmentTransaction fTrans;
    SettingActivity frag1;
    private TextView mTextViewDelete;
    private TextView mTextViewRate;
    private TextView mTextViewVersion;
    private TextView mTextViewAboutVersion;

    public AllSettingActivity(final View view, Context context) {
        mDbHelper1 = new OsmDbHelper(view.getContext());
        mDbHelper2 = new TrainingDbHelper(view.getContext());
        mDbHelper3 = new KigDbHelper(view.getContext());
        mTextViewDelete = (TextView) view.findViewById(R.id.textViewDelete);
        mTextViewRate = (TextView) view.findViewById(R.id.textViewRate);
        mTextViewVersion = (TextView) view.findViewById(R.id.textViewVersion);
        mTextViewAboutVersion = (TextView) view.findViewById(R.id.textViewAboutVersion);
        mTextViewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeAll();
                Toast.makeText(view.getContext(),"Все записи удалены!" +
                        "", Toast.LENGTH_SHORT).show();
            }
        });
        mTextViewAboutVersion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(view.getContext(), AboutVersion.class);
                view.getContext().startActivity(intent);
            }
        });
        mTextViewRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new
                        Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.smartglovealpha"));
                view.getContext().startActivity(browserIntent);
            }
        });
    }
    public void removeAll()
    {
        // db.delete(String tableName, String whereClause, String[] whereArgs);
        // If whereClause is null, it will delete all rows.
        SQLiteDatabase db = mDbHelper1.getWritableDatabase(); // helper is object extends SQLiteOpenHelper
        db.delete(TrainingContract.OSM.TABLE_NAME2, null, null);
        SQLiteDatabase db1 = mDbHelper2.getWritableDatabase(); // helper is object extends SQLiteOpenHelper
        db1.delete(TrainingContract.Notes.TABLE_NAME, null, null);
        SQLiteDatabase db2 = mDbHelper3.getWritableDatabase(); // helper is object extends SQLiteOpenHelper
        db2.delete(TrainingContract.KIGNotes.TABLE_NAME3, null, null);
    }
}
