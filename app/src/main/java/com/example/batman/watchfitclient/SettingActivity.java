package com.example.batman.watchfitclient;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.batman.watchfitclient.Data.data.InformationDbHelper;
import com.example.batman.watchfitclient.Data.data.OsmDbHelper;
import com.example.batman.watchfitclient.Data.data.TrainingContract;
import com.example.batman.watchfitclient.Data.data.TrainingDbHelper;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;


public class SettingActivity {
    private EditText EditTextAge;
    private EditText EditTextHeigh;
    private EditText EditTextWeight;
    String Name;
    int Gender;
    // private TextView display;
    //private TextView display1;
    private Button mButton;
    public TrainingDbHelper mDbHelper;
    private InformationDbHelper mDbHelper1;
    private OsmDbHelper mDbHelper2;
    RadioButton mradioButtonM;
    RadioButton mradioButtonW;
    StorageReference riversRef;
    private StorageReference mStorageRef;
    FirebaseUser user;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    public SettingActivity(final View view, final Context context) {
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    Name = user.getEmail();
                    Toast.makeText(view.getContext(),Name,Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(view.getContext(), "Нет соединения с интернетом, или вы не авторизированы!", Toast.LENGTH_SHORT).show();
                }
            }
        };
        mAuth.addAuthStateListener(mAuthListener);

        RadioGroup radioGroup = (RadioGroup) view.findViewById(R.id.RadioGroup);
        mradioButtonM = (RadioButton) view.findViewById(R.id.radioButtonM);
        mradioButtonW = (RadioButton) view.findViewById(R.id.radioButtonW);
        //display = (TextView) rootView.findViewById(R.id.textView25);
        //display1 = (TextView) rootView.findViewById(R.id.textView26);
        mButton = (Button) view.findViewById(R.id.button7);
        EditTextWeight = (EditText) view.findViewById(R.id.editTextWeight);
        EditTextHeigh = (EditText) view.findViewById(R.id.editTextHeigh);
        EditTextAge = (EditText) view.findViewById(R.id.editTextAge);
        mDbHelper = new TrainingDbHelper(view.getContext());
        mDbHelper1 = new InformationDbHelper(view.getContext());
        mDbHelper2 = new OsmDbHelper(view.getContext());
        //displayDatabaseInfoTraining();
        //displayDatabaseInfoOsm();
        displayDatabaseInfo();
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioButtonM: Gender = 1;
                        break;
                    case R.id.radioButtonW: Gender =2;
                        break;
                    default:
                        break;
                }
            }
        });
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateInf();
                displayDatabaseInfo();
                mStorageRef = FirebaseStorage.getInstance().getReference();
                riversRef = mStorageRef.child(Name + "/inf.db");
                upload(view,context);
                //Intent intent = new Intent(SettingActivity.this, MainActivity.class);
                //startActivity(intent);
                //finish();
            }
        });
    }
    /*
    public void displayDatabaseInfoTraining() {
        Cursor cursor = this.mDbHelper.getReadableDatabase().query(TrainingContract.Notes.TABLE_NAME, new String[]{"_id", TrainingContract.Notes.COLUMN_PULSE, TrainingContract.Notes.COLUMN_TIME, TrainingContract.Notes.COLUMN_ENERGY}, null, null, null, null, null);
        try {
            display.setText(cursor.getCount() + " измерений\n\n");
            display.append("Номер - Пульс - Время - Энергозатратность\n");
            int idColumnIndex = cursor.getColumnIndex("_id");
            int pulseColumnIndex = cursor.getColumnIndex(TrainingContract.Notes.COLUMN_PULSE);
            int timeColumnIndex = cursor.getColumnIndex(TrainingContract.Notes.COLUMN_TIME);
            int energyColumnIndex = cursor.getColumnIndex(TrainingContract.Notes.COLUMN_ENERGY);
            while (cursor.moveToNext()) {
                int currentID = cursor.getInt(idColumnIndex);
                int currentPulse = cursor.getInt(pulseColumnIndex);
                int currentTime = cursor.getInt(timeColumnIndex);
                display.append("\n" + currentID + " - " + currentPulse + " уд - " + currentTime + " сек - " + cursor.getInt(energyColumnIndex) + " Кал ");
            }
        } finally {
            cursor.close();
        }
    }
    public void displayDatabaseInfoOsm() {
        Cursor cursor = this.mDbHelper2.getReadableDatabase().query(TrainingContract.OSM.TABLE_NAME2, new String[]{"_id", TrainingContract.OSM.COLUMN_PSIT, TrainingContract.OSM.COLUMN_PSTAND, TrainingContract.OSM.COLUMN_POINT, TrainingContract.OSM.COLUMN_ZONE}, null, null, null, null, null);
        try {
            display1.setText(cursor.getCount() + " измерений\n\n");
            display1.append("Номер - ЧСС сидя - ЧСС стоя - Балл - Зона\n");
            int idColumnIndex = cursor.getColumnIndex("_id");
            int psitColumnIndex = cursor.getColumnIndex(TrainingContract.OSM.COLUMN_PSIT);
            int pstandColumnIndex = cursor.getColumnIndex(TrainingContract.OSM.COLUMN_PSTAND);
            int pointColumnIndex = cursor.getColumnIndex(TrainingContract.OSM.COLUMN_POINT);
            int zoneColumnIndex = cursor.getColumnIndex(TrainingContract.OSM.COLUMN_ZONE);
            while (cursor.moveToNext()) {
                int currentID = cursor.getInt(idColumnIndex);
                int currentPsit = cursor.getInt(psitColumnIndex);
                int currentPstand = cursor.getInt(pstandColumnIndex);
                int currentPoint = cursor.getInt(pointColumnIndex);
                display1.append("\n" + currentID + " - " + currentPsit + " уд - " + currentPstand + " уд - " + currentPoint + " баллов - " + cursor.getInt(zoneColumnIndex) + " зона ");
            }
        } finally {
            cursor.close();
        }
    }
    */
    public void displayDatabaseInfo() {
        Cursor cursor = mDbHelper1.getReadableDatabase().query(TrainingContract.Information.TABLE_NAME1, new String[]{"_id", TrainingContract.Information.COLUMN_WEIGHT, TrainingContract.Information.COLUMN_HEIGH, TrainingContract.Information.COLUMN_AGE, TrainingContract.Information.COLUMN_GENDER}, null, null, null, null, "_id DESC");
        try {
            if (cursor.getCount() == 0) {
                insertInf();
                displayDatabaseInfo();
            }
            int idColumnIndex = cursor.getColumnIndex("_id");
            int weightColumnIndex = cursor.getColumnIndex(TrainingContract.Information.COLUMN_WEIGHT);
            int heighColumnIndex = cursor.getColumnIndex(TrainingContract.Information.COLUMN_HEIGH);
            int ageColumnIndex = cursor.getColumnIndex(TrainingContract.Information.COLUMN_AGE);
            int genderColumnIndex = cursor.getColumnIndex(TrainingContract.Information.COLUMN_GENDER);
            while (cursor.moveToNext()) {
                int currentID = cursor.getInt(idColumnIndex);
                int currentWeight = cursor.getInt(weightColumnIndex);
                int currentHeigh = cursor.getInt(heighColumnIndex);
                int currentAge = cursor.getInt(ageColumnIndex);
                int currentGender = cursor.getInt(genderColumnIndex);
                EditTextWeight.setText(currentWeight + "");
                EditTextHeigh.setText(currentHeigh + "");
                EditTextAge.setText(currentAge + "");
                if (currentGender == 1) {
                    this.mradioButtonM.setChecked(true);
                }
                if (currentGender == 2) {
                    this.mradioButtonW.setChecked(true);
                }
            }
        } finally {
            cursor.close();
        }
    }
    private void UpdateInf() {
        SQLiteDatabase db = this.mDbHelper1.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TrainingContract.Information.COLUMN_WEIGHT, Integer.valueOf(Integer.parseInt(this.EditTextWeight.getText().toString())));
        values.put(TrainingContract.Information.COLUMN_HEIGH, Integer.valueOf(Integer.parseInt(this.EditTextHeigh.getText().toString())));
        values.put(TrainingContract.Information.COLUMN_AGE, Integer.valueOf(Integer.parseInt(this.EditTextAge.getText().toString())));
        if (this.mradioButtonM.isChecked()) {
            values.put(TrainingContract.Information.COLUMN_GENDER, Integer.valueOf(1));
        }
        if (this.mradioButtonW.isChecked()) {
            values.put(TrainingContract.Information.COLUMN_GENDER, Integer.valueOf(2));
        }
        long newRowId = (long) db.update(TrainingContract.Information.TABLE_NAME1, values, null, null);
    }

    private void insertInf() {
        SQLiteDatabase db = this.mDbHelper1.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TrainingContract.Information.COLUMN_WEIGHT, Integer.valueOf(0));
        values.put(TrainingContract.Information.COLUMN_HEIGH, Integer.valueOf(0));
        values.put(TrainingContract.Information.COLUMN_AGE, Integer.valueOf(0));
        values.put(TrainingContract.Information.COLUMN_GENDER, Integer.valueOf(0));
        long newRowId = db.insert(TrainingContract.Information.TABLE_NAME1, null, values);
    }
    public void upload(final View view, Context context) {

        final Uri file = Uri.fromFile(new File(Environment.getDataDirectory(), "//data//" + "com.example.batman.watchfitclient"
                + "//databases//" + "inf.db"));
        Toast.makeText(view.getContext(), "Идет загрузка", Toast.LENGTH_SHORT).show();
        riversRef.putFile(file)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // Get a URL to the uploaded content
                        Toast.makeText(view.getContext(), "Загрузка завершена успешно!", Toast.LENGTH_SHORT).show();
                        mAuth.removeAuthStateListener(mAuthListener);
                        //Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        Toast.makeText(view.getContext(), "Ошибка загрузки данных!", Toast.LENGTH_SHORT).show();
                        // ...
                    }
                });
    }
}