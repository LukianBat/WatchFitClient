package com.example.batman.watchfitclient;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.batman.watchfitclient.Data.data.TrainingContract;
import com.example.batman.watchfitclient.Data.data.TrainingDbHelper;

import java.util.ArrayList;
import java.util.List;

public class RecycleViewActivityTr {
    private TrainingDbHelper mDbHelper;
    private List<TrainingData> TrainingDatas;
    private RecyclerView rv;
    TextView mTextView;

    public RecycleViewActivityTr(View view, Context context) {
        rv=(RecyclerView)view.findViewById(R.id.rv2);
        mTextView = (TextView) view.findViewById(R.id.textView22);
        LinearLayoutManager llm = new LinearLayoutManager(view.getContext());
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);
        mDbHelper = new TrainingDbHelper(view.getContext());
        initializeData();
        initializeAdapter();
    }

    private void initializeData(){
        TrainingDatas = new ArrayList<>();
        Cursor cursor = this.mDbHelper.getReadableDatabase().query(TrainingContract.Notes.TABLE_NAME, new String[]{"_id", TrainingContract.Notes.COLUMN_PULSE, TrainingContract.Notes.COLUMN_TIME, TrainingContract.Notes.COLUMN_ENERGY, TrainingContract.OSM.COLUMN_DATE}, null, null, null, null, null);
        try {
            //   display1.setText(cursor.getCount() + " измерений\n\n");
            //   display1.append("Номер - ЧСС сидя - ЧСС стоя - Балл - Зона\n");
            int idColumnIndex = cursor.getColumnIndex("_id");
            int pulseColumnIndex = cursor.getColumnIndex(TrainingContract.Notes.COLUMN_PULSE);
            int timeColumnIndex = cursor.getColumnIndex(TrainingContract.Notes.COLUMN_TIME);
            int energyColumnIndex = cursor.getColumnIndex(TrainingContract.Notes.COLUMN_ENERGY);
            int dateColumnIndex = cursor.getColumnIndex(TrainingContract.Notes.COLUMN_DATE);
            if (cursor.getCount() != 0) {
                while (cursor.moveToNext()) {
                    int currentPulse = cursor.getInt(pulseColumnIndex);
                    int currentTime = cursor.getInt(timeColumnIndex);
                    int currentEnergy = cursor.getInt(energyColumnIndex);
                    String currentDate = cursor.getString(dateColumnIndex);
                    TrainingDatas.add(new TrainingData(currentPulse, currentTime, currentEnergy, R.drawable.fp2, currentDate));
                    //          display1.append("\n" + currentID + " - " + currentPsit + " уд - " + currentPstand + " уд - " + currentPoint + " баллов - " + cursor.getInt(zoneColumnIndex) + " зона ");
                }
            }
            else if (cursor.getCount() == 0){
                mTextView.setVisibility(View.VISIBLE);
            }
        } finally {
            cursor.close();
        }
    }

    private void initializeAdapter(){
        RVAdapterTraining adapter = new RVAdapterTraining(TrainingDatas);
        rv.setAdapter(adapter);
    }
}
