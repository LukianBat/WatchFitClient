package com.example.batman.watchfitclient;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import devlight.io.library.ntb.NavigationTabBar;

public class MainActivity extends AppCompatActivity {

    Context context;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_main);
        initUI();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void initUI() {
        final ViewPager viewPager = findViewById(R.id.vp_horizontal_ntb);
        viewPager.setAdapter(new PagerAdapter() {
            @Override
            public int getCount() {
                return 5;
            }

            @Override
            public boolean isViewFromObject(final View view, final Object object) {
                return view.equals(object);
            }

            @Override
            public void destroyItem(final View container, final int position, final Object object) {
                ((ViewPager) container).removeView((View) object);
            }

            @Override
            public Object instantiateItem(final ViewGroup container, final int position) {
                View view =  LayoutInflater.from(
                        getBaseContext()).inflate(R.layout.activity_setting, null, false);
                switch (position) {
                    case 0:
                        view = LayoutInflater.from(getBaseContext()).inflate(R.layout.login_activity, null, false);
                        new LogInActivity(view, context,viewPager, container);
                        break;
                    case 1:
                            view = LayoutInflater.from(getBaseContext()).inflate(R.layout.activity_recycle_view_osm, null, false);
                            new RecycleViewActivityOsm(view, context);
                            break;
                    case 2:
                            view = LayoutInflater.from(getBaseContext()).inflate(R.layout.activity_setting, null, false);
                            new SettingActivity(view, context);

                        break;
                    case 3:
                        view = LayoutInflater.from(
                                getBaseContext()).inflate(R.layout.activity_recycle_view_tr, null, false);
                                new RecycleViewActivityTr(view,context);
                        break;
                    case 4:
                        view = LayoutInflater.from(
                                getBaseContext()).inflate(R.layout.activity_all_setting, null, false);
                                new AllSettingActivity(view, context);

                        break;
                }
                container.addView(view);
                return view;
            }
        });
        final String[] colors = getResources().getStringArray(R.array.default_preview);
        final NavigationTabBar navigationTabBar = findViewById(R.id.ntb_horizontal);
        final ArrayList<NavigationTabBar.Model> models = new ArrayList<>();
        navigationTabBar.setBgColor(getColor(R.color.colorPrimary));

        navigationTabBar.setInactiveColor(getColor(R.color.cardview_light_background));
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_news),
                        Color.parseColor(colors[0]))
                        .title("Авторизация")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_stack),
                        Color.parseColor(colors[1]))
                        .title("ОСМ")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_prof),
                        Color.parseColor(colors[2]))
                        .title("Профиль")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_stat),
                        Color.parseColor(colors[3]))
                        .title("Тренировки")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_settings),
                        Color.parseColor(colors[4]))
                        .title("Настройки")
                        .build()
        );
        navigationTabBar.setModels(models);
        navigationTabBar.setViewPager(viewPager, 2);
        navigationTabBar.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(final int position) {
                navigationTabBar.getModels().get(position).hideBadge();
            }

            @Override
            public void onPageScrollStateChanged(final int state) {

            }
        });

        navigationTabBar.postDelayed(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < navigationTabBar.getModels().size(); i++) {
                    final NavigationTabBar.Model model = navigationTabBar.getModels().get(i);
                    navigationTabBar.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            model.showBadge();
                        }
                    }, i * 100);
                }
            }
        }, 500);
    }
}
