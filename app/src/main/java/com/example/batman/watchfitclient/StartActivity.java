package com.example.batman.watchfitclient;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;

public class StartActivity extends Activity {

    StorageReference riversRef;
    StorageReference riversRef3;
    StorageReference riversRef4;
    String Name;
    ConstraintLayout layout;
    FirebaseUser user;
    ImageView logo;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private StorageReference mStorageRef;
    private String TAG= "TAG";

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }
    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //layout = findViewById(R.id.start);
        logo = findViewById(R.id.imageView);
        //
        // logo.setImageResource(R.drawable.side_nav_bar);

        setContentView(R.layout.activity_start);
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    Name = user.getEmail();
                    Toast.makeText(StartActivity.this,Name,Toast.LENGTH_SHORT).show();
                    mStorageRef = FirebaseStorage.getInstance().getReference();
                    riversRef = mStorageRef.child(Name + "/inf.db");
                    riversRef3 = mStorageRef.child(Name + "/training.db");
                    riversRef4 = mStorageRef.child(Name + "/osm.db");
                    Intent i = new Intent(StartActivity.this,MainActivity.class);
                    startActivity(i);
                    finish();
                    try {
                        download();
                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.i(TAG, e.toString());
                    }
                } else {
                    Toast.makeText(StartActivity.this, "Нет соединения с интернетом, или вы не авторизированы!", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(StartActivity.this,MainActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        };

    }
    public void download() throws IOException {
        Toast.makeText(StartActivity.this, "Идет загрузка", Toast.LENGTH_SHORT).show();
        final File localFile = new File(Environment.getDataDirectory(), "//data//" + "com.example.batman.watchfitclient"
                + "//databases//" + "inf.db");
        final File localFile3 = new File(Environment.getDataDirectory(), "//data//" + "com.example.batman.watchfitclient"
                + "//databases//" + "training.db");
        final File localFile4 = new File(Environment.getDataDirectory(), "//data//" + "com.example.batman.watchfitclient"
                + "//databases//" + "osm.db");
        riversRef.getFile(localFile)
                .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                        Toast.makeText(StartActivity.this, "Загрузка данных завершена!", Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle failed download
                // ...
                Toast.makeText(StartActivity.this, exception.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        riversRef3.getFile(localFile3)
                .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                        // Successfully downloaded data to local file
//                        Intent i = new Intent(SucEnter.this, ExportImportDB.class);
//                        i.putExtra("op", "imp");
//                        startActivity(i);
                        Toast.makeText(StartActivity.this, "Загрузка данных завершена!", Toast.LENGTH_SHORT).show();
                        // ...
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle failed download
                // ...
                Toast.makeText(StartActivity.this, exception.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        riversRef4.getFile(localFile4)
                .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                        // Successfully downloaded data to local file
//                        Intent i = new Intent(SucEnter.this, ExportImportDB.class);
//                        i.putExtra("op", "imp");
//                        startActivity(i);
                        Toast.makeText(StartActivity.this, "Загрузка данных завершена!", Toast.LENGTH_SHORT).show();
                        // ...
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle failed download
                // ...
                Toast.makeText(StartActivity.this, exception.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}