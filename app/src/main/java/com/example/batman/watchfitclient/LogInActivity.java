package com.example.batman.watchfitclient;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import devlight.io.library.ntb.NavigationTabBar;


public class LogInActivity {
    String action = "auth";
    FirebaseUser user;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private EditText Email;
    private EditText Password;
    Button regBut;
    Button logBut;

    public LogInActivity(final View view, final Context context, final ViewPager viewPager, final ViewGroup container) {

 /*   @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }
    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }*/
        final NavigationTabBar navigationTabBar = view.findViewById(R.id.ntb_horizontal);
        regBut = view.findViewById(R.id.btn_registration);
        logBut = view.findViewById(R.id.btn_sign_in);
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    String email = user.getEmail();
                    Intent i = new Intent(view.getContext(), SucEnter.class);
                    i.putExtra("name", email);
                    i.putExtra("action", action);
                    view.getContext().startActivity(i);
                    // User is signed in
                } else {
                    // User is signed out
                }
                // ...
            }
        };
        mAuth.addAuthStateListener(mAuthListener);
        Email = (EditText) view.findViewById(R.id.et_email);
        Password = (EditText) view.findViewById(R.id.et_password);
        regBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(view.getContext(), RegistrationActivity.class);
                view.getContext().startActivity(i);
            }
        });
        logBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Email.toString() != null && Password.toString() != null){
                    SignIn(Email.getText().toString(), Password.getText().toString(), view);
                }
            }
        });
    }

    public void SignIn(String email, String password, final View view) {
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener((Activity) view.getContext(), new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(view.getContext(), "Вход выполнен!", Toast.LENGTH_SHORT).show();
                    //Intent i = new Intent(view.getContext(), SucEnter.class);
                    //i.putExtra("name", Email.getText().toString());
                    //i.putExtra("pass", Password.getText().toString());
                    //view.getContext().startActivity(i);
                    mAuth.removeAuthStateListener(mAuthListener);
                } else {
                    Toast.makeText(view.getContext(), "Ошибка входа!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}